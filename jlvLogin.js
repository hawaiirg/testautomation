
var casper = require('casper').create({
    verbose: true,
    logLevel: "debug"
});

var jlvURL = 'http://10.2.100.100/JLV/';
var imageFolder = './screenshots/';

//Page Load Check
casper.start(jlvURL, function() {
    this.capture(imageFolder +'JLVLogin.jpg', undefined, {
        format: 'jpg',
        quality: 75
    });
    this.echo('[Agreement Check]');
    //Waits for the warning page to load
    if(this.exists('#Agreement')){
        this.echo('Agreement shows up');
    } else {
        this.echo('Agreement does not show up');
    }
    if(this.exists('.acceptButton')){
        this.click('.acceptButton');
    } else {
        this.echo('** Accept Button is not there **');
    }
});

//Check for System Issues and Prints them
casper.then(function () {
    this.echo('[System Check]');
    if(this.exists('#LoginHeartbeat ul li')){
        this.echo('System Status : ');
        this.echo(this.fetchText('#LoginHeartbeat ul li'));
    }
    //TODO: screen capture the system issues
});

//Fills out the form and submits it
casper.then(function () {
    this.echo('[Submitting JLV Login credentials]');
    this.fill('form[action="/JLV/"]', { accessCode: 'pu1234',
        verifyCode: 'pu1234!!', loginAgency: 'VA', siteCode: '200' }, true);
    this.echo('Page URL : ' + this.getCurrentUrl());
});

//Check navigation works and search after form submit
casper.then(function() {
    this.echo('[Checking Patient Search shows up]');

    //Gives the Patient Search Field a 10 second timeout to load
    this.waitFor(function check(){
        return (this.getCurrentUrl() === jlvURL + 'app/index');
    },
    function then() {
        this.echo('Page Navigation Succesful');
    },
    function timeout(){
        this.echo('Page Navigation Unsuccesful');
    },
    10000);

    this.capture(imageFolder + 'JLVPatientSearch.jpg', undefined, {
        format: 'jpg',
        quality: 75
    });

    // this.echo('Page URL : ' + this.getCurrentUrl());
    if(this.exist('#PatientSearchDialog')){
        this.echo('Patient Dialog Box Shows up');
    }
});

// var patientID = '0000000001';
// //Fill out the patient search box
// casper.thenEvaluate(function(patientID) {
//     this.echo('[Input Patient ID parameters]');
//     document.querySelector('input[id="PatientSearchEDIPIInput"]').setAttribute('value', patientID);
//     document.querySelector('div[id="PatientSearchButton"]').click();
// }, 'CasperJS');

// casper.then(function() {
//     this.wait(15000, function() {
//         this.echo('The search results should be populated right now');
//     });
// });


casper.run();
